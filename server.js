const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const admin = require('firebase-admin');
const https = require('https');
var http = require('http');
const fs = require('fs');

const serviceAccount = require('./key.json');

const httpsOptions = {
   key: fs.readFileSync('./certs/key.pem', 'utf8'),
   cert: fs.readFileSync('./certs/server.crt', 'utf8')
 };

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

const collectionRef = db.collection('destinations');

app.use(express.static("views"));
app.use(bodyParser.json());
app.get("/", function(request, response) {
  response.sendFile(__dirname + "/views/index.html");
});

app.get('/getAllDestinations', async (req, res) => {
  try {
      const snapshot = await collectionRef.get();
      const data = [];
      snapshot.forEach((doc) => {
          data.push(doc.data());
      });
      res.json(data);
  } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred' });
  }
});

app.post('/generateDestinations', async (req, res) => {
  try {
      const requestData = req.body;
      let query = db.collection('destinations');
      for (const key in requestData) {
        if (requestData.hasOwnProperty(key) && requestData[key] !== '') {
            query = query.where(key, '==', requestData[key]);
        }
      }
      const querySnapshot = await query.get();
      const data = [];
      querySnapshot.forEach((doc) => {
          data.push(doc.data());
      });
      res.json(data);
  } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred' });
  }
});

app.post('/login', async (req, res) => {
  try {
      const requestData = req.body;
      let query = db.collection('users');
      for (const key in requestData) {
        if (requestData.hasOwnProperty(key) && requestData[key] !== '') {
            query = query.where(key, '==', requestData[key]);
        }
      }
      const querySnapshot = await query.get();
      const data = [];
      querySnapshot.forEach((doc) => {
          data.push(doc.data());
      });
      res.json(data);
  } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred' });
  }
});

app.post('/createLogin', async (req, res) => {
  try {
      const requestData = req.body;
      let query = db.collection('users');
      query.add(requestData)
      res.status(200).json({"message": "success"});
  } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred' });
  }
});

const securePort = process.env.PORT || 443;
const openPort = process.env.PORT || 443;

 const httpsServer = https.createServer(httpsOptions, app);
//const httpServer = http.createServer(app);

 const securelistener = httpsServer.listen(securePort, function() {
   console.log("Your app is listening on port " + securelistener.address().port);
 });

//const securelistener = httpServer.listen(openPort, function() {
//    console.log("Your app is listening on port " + securelistener.address().port);
//  });
  
  
